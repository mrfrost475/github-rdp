# GitHub-RDP
This repository will allow you to rent any VPS you like for free on Windows, Linux and MacOS

## Machine Specifications:
### Windows and Linux:
- Intel(R) Xeon(R) Platinum 8370C CPU @ 2.80 GHz (2x vCPU)
- 7.00 GB RAM
- 256.00 GB SSD NVMe
### MacOS 10.15 - 12.6.1
- Intel(R) Xeon(R) E5-1650 v2 CPU @ 3.50 GHz (3x vCPU)
- 14.30 GB RAM
- 380.00 GB SSD NVMe **(/home partition free 14.00 GB)**

### How long does the server run? ###
- MacOS VM runs up to 6 hours
- Windows and Linux VMs work up to 45-60 minutes

## Setting up:
1. Fork this project to your GitHub Account
2. Go to your peoject page, click `Settings` and go to `Secrets`, and then click `New Secret` to add these secrets below:

Secrets Name | Uses | Notes
----- | ----- | -----
`MAC_REALNAME` | For MacOS User Display Name | Type any name you want
`MAC_USER_PASSWORD` | For MacOS System Admin Password | Type any password you want
`NGROK_AUTH_TOKEN` | For **ngrok** tunnel uses | Go to website, and copy the API key from https://dashboard.ngrok.com/auth/your-authtoken
`VNC_PASSWORD` | For the login password of VNC remote authentication | Type any password you want
`LINUX_USERNAME` | For linux system username | Type any name you want
`LINUX_USER_PASSWORD` | For linux shell and root password | Type any password you want
`LINUX_MACHINE_NAME` | For Linux System Computer name | Type any name you want
`CHROME_HEADLESS_CODE` | For remoting linux desktop using google remote | Copy Codes from [here](https://remotedesktop.google.com/headless) and login with your google account, and then copy the code below `Debian Linux` blank. :warning: Each code can only be used for once, generate another code when u have used that one.

### Thanks very mych to ###
- mrfrost475 - (Main idea)
- coolhazer - (Assistance in implementation)
- vextaM - (Running macOS and Linux)
- FeruzRus - (Starting Windows via CRD)
- Edward Martell - (MacOS Launch Information)
- Aftab Tufail - (Easy way to start windows with ngrok)

### Attention !!! (Read and understand before use) ###
- Your GitHub account may be permanently deleted or blocked, be aware of this!!!
- All contents of this repository are provided for informational purposes only !!!
- The tools in this repository should only be used for EDUCATIONAL or INSTRUCTIONAL, WORKING PURPOSES !!!
- Under no circumstances should this repository be used for MINING, SCAM, FRAUD or ANY OTHER UNLAWFUL PURPOSES !!!
- It is not recommended to abuse the very frequent launch of virtual machines, you may be banned or black labeled !!!
- THE AUTHOR CREATED THIS REPOSITORY TO HELP OTHER PEOPLE TO USE THE GITHUB ACTIONS FOR REALLY IMPORTANT TASKS AND DESPECT ANYONE WHO ABUSE THE AUTHOR'S TRUST IN ANY WAY !!!
